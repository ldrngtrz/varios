package poli;

public class Main {

	public static void main(String[] args) {
		Animal myAnimal = new Animal(); // Create a Animal object
		Animal perro = new Perro(); // Create a Dog object
		Animal gato  = new Gato(); // Create a Pig object
		Animal marrano = new Marrano(); // Create a Pig object
		myAnimal.animalSound();
		perro.animalSound();
		gato.animalSound();
		marrano.animalSound();
	}
}
