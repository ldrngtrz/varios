package q1;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Set;

public class Arbol {
	HashMap<String, ArrayList<String>> arbol = new HashMap<>();
	int nivel = 0;

	public boolean isFullBinary() {
		boolean retval = true;
		Integer numHijos = 0;
		Set<String> lst = arbol.keySet();
		for (String item : lst) {
			if (!item.equals("root")) {
				List<String> lst2 = arbol.get(item);
				numHijos = 0;
				for (String item2 : lst2) {
					numHijos++;
				}
				//System.out.println("numHijos : " + numHijos);
				if ((numHijos != 0) && (numHijos != 2)) {
					//System.out.println("dentro : ");
					retval = false;
					break;
				}
			}
		}
		return retval;
	}

	public String obtenerPadre(String hijo) {
		String retval = "NE";
		Set<String> lst = arbol.keySet();
		for (String item : lst) {
			List<String> lst2 = arbol.get(item);
			for (String item2 : lst2) {
				if (item2.equals(hijo)) {
					retval = item;
					break;
				}
				if (!retval.equals("NE")) {
					break;
				}
			}
		}
		return retval;
	}

	public void agregarNodo(String nombreNodo, String parent) {
		arbol.put(nombreNodo, new ArrayList<String>());
		if (parent != null) {
			arbol.get(parent).add(nombreNodo);
		}
	}

	public void graficar(String nodoIni, int nivel) {
		String espacios = "";
		for (int i = 0; i < nivel; i++) {
			espacios = espacios.concat("  ");
		}
		System.out.println(espacios + nodoIni);
		nivel++;
		for (String hijo : arbol.get(nodoIni)) {
			this.graficar(hijo, nivel);
		}
	}

	public static void main(String[] args) {
		Arbol arbol = new Arbol();
		arbol.agregarNodo("root", null);
		arbol.agregarNodo("dir1", "root");
		arbol.agregarNodo("dir2", "root");
		arbol.agregarNodo("dir3", "root");
		arbol.agregarNodo("imagen1", "dir1");
		arbol.agregarNodo("juicio de paris", "imagen1");
		arbol.agregarNodo("la creacion", "imagen1");
		arbol.agregarNodo("imagen2", "dir1");
		arbol.agregarNodo("imagen3", "dir1");
		arbol.agregarNodo("monalisa", "imagen3");
		arbol.agregarNodo("girasoles", "imagen3");
		arbol.agregarNodo("script1", "dir2");
		arbol.agregarNodo("script2", "dir2");
		arbol.agregarNodo("script3", "dir2");
		arbol.agregarNodo("doc1", "dir3");
		arbol.agregarNodo("doc2", "dir3");
		arbol.agregarNodo("doc3", "dir3");
		arbol.agregarNodo("dante", "doc3");
		arbol.graficar("root", 0);
		String padre = arbol.obtenerPadre("monalisa");
		System.out.println("padre de monalisa : " + padre);

	}
}
