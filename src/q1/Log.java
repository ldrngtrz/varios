package q1;

public class Log implements Comparable<Log> {

	String tipo;
	String desc;
	String timestamp;

	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	public String getDesc() {
		return desc;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}

	public String getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(String timestamp) {
		this.timestamp = timestamp;
	}

	@Override
	public int compareTo(Log log2) {

		return this.getTimestamp().compareTo(log2.getTimestamp());
	}

}
