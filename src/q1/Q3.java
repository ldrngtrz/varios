package q1;

public class Q3 {

	public static void main(String[] args) {
//		System.out.println("ini");
		String a1 = "1(2(4(),5()),3())";
		String a2 = "1(2(4(),),3())";
		String a3 = "1(2(4(),5()),7(),3())";
		String e1 = "";
		String e2 = "";
		String e3 = "";
		String estado = "inicio";
		String padre = "";
		String arbol = a3;
		String item = "";
		String itemAnt = "";
		Arbol arb = new Arbol();
		arb.agregarNodo("root", null);
		padre = "root";
		for (int i = 0; i < arbol.length(); i++) {
			item = arbol.substring(i, i + 1);
//			System.out.println(item);
			if (item.equals("(")) {
				padre=itemAnt;
			} else if (item.equals(")")) {
				padre = arb.obtenerPadre(padre);
			} else if (item.equals(",")) {
				estado = "hermano";
			} else {
				if (estado.equals("inicio")) {
					estado = "root";
				} 
				System.out.println("item : "+item+"  padre : "+padre);
				arb.agregarNodo(item, padre);
			}
			itemAnt=item;

		}
		arb.graficar("root",0);
	}
}
