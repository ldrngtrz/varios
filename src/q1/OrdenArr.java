package q1;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class OrdenArr {

		public static void main(String[] args) {
			List<Log> lst = new ArrayList<Log>();
			String[] partes = new String[3];
			String[] info = { 
					"DEBUG com.fastspring.core.persistence.TransactionContext 1592777662",
			    "INFO co.fastspring.tms.services.background.purge.PurgeManager 1592777661",
			    "WARNING co.fastspring.tms.services.background.tc.Connector 1592777663",
			    "DEBUG com.fastspring.core.persistence.Transaction 1592777112",
			    "INFO co.fastspring.tms.services.background.purge.PurgeIterator 1592777000",
			    "INFO co.fastspring.tms.services.background.purge.PurgeApp 1592777345",
			    "WARNING com.fastspring.core.persistence.TransactionManager 1592777245",
			    "INFO co.fastspring.tms.services.background.lab.Factory 1592777350",
			    "WARNING co.fastspring.tms.services.background.purge.Iterator 1592777880",
			    "INFO com.fastspring.core.persistence.TransactionContext 1592777320",
			    "DEBUG co.fastspring.tms.services.background.context.Scheduler 1592777224",
			    "DEBUG co.fastspring.tms.services.background.purge.PurgeApp 1592777450",
			    "WARNING com.fastspring.core.persistence.TransactionManager 1592777333",
			    "INFO co.fastspring.tms.services.background.purge.PurgeManager 1592777665",
			    "WARNING co.fastspring.tms.services.background.purge.Integrator 1592777543" };

			for (int i = 0; i < info.length; i++) {
					partes = info[i].split(" ");
					Log log = new Log();
					log.setTipo(partes[0]);
					log.setDesc(partes[1]);
					log.setTimestamp(partes[2]);
					lst.add(log);
				}
		
		for (Log log:lst) {
				System.out.println(log.getTipo()+" "+log.getDesc()+" "+log.getTimestamp());
			}
		System.out.println("------------------------------");
		Collections.sort(lst);
		for (Log log:lst) {
			System.out.println(log.getTipo()+" "+log.getDesc()+" "+log.getTimestamp());
		}

		
		}
	}
