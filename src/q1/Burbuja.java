package q1;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class Burbuja {

	public static void main(String[] args) {
		String tempo = "";
		String[] partes = new String[3];
		String[] partes2 = new String[3];
		String[] info = { 
				"DEBUG com.fastspring.core.persistence.TransactionContext 1592777662",
		    "INFO co.fastspring.tms.services.background.purge.PurgeManager 1592777661",
		    "WARNING co.fastspring.tms.services.background.tc.Connector 1592777663",
		    "DEBUG com.fastspring.core.persistence.Transaction 1592777112",
		    "INFO co.fastspring.tms.services.background.purge.PurgeIterator 1592777000",
		    "INFO co.fastspring.tms.services.background.purge.PurgeApp 1592777345",
		    "WARNING com.fastspring.core.persistence.TransactionManager 1592777245",
		    "INFO co.fastspring.tms.services.background.lab.Factory 1592777350",
		    "WARNING co.fastspring.tms.services.background.purge.Iterator 1592777880",
		    "INFO com.fastspring.core.persistence.TransactionContext 1592777320",
		    "DEBUG co.fastspring.tms.services.background.context.Scheduler 1592777224",
		    "DEBUG co.fastspring.tms.services.background.purge.PurgeApp 1592777450",
		    "WARNING com.fastspring.core.persistence.TransactionManager 1592777333",
		    "INFO co.fastspring.tms.services.background.purge.PurgeManager 1592777665",
		    "WARNING co.fastspring.tms.services.background.purge.Integrator 1592777543" };

		for (int i = 0; i < info.length; i++) {
			for (int j = 0; j < info.length-1; j++) {
				partes = info[j].split(" ");
				partes2 = info[j + 1].split(" ");
				if (partes[0].compareTo(partes2[0]) > 0) {
					if (partes[0].lastIndexOf("INFO") >= 0) {
					}
					tempo = info[j];
					info[j] = info[j + 1];
					info[j + 1] = tempo;
				}
			}
		}
		for (int k = 0; k < info.length; k++) {
			System.out.println(info[k]);
		}

		List<BigDecimal> list = Arrays.asList(new BigDecimal("24.455"), new BigDecimal("23.455"), new BigDecimal("28.455"),
		    new BigDecimal("20.455"));
//			System.out.println("Unsorted list: " + list);
		final List<BigDecimal> sortedList = list.stream().sorted((o1, o2) -> o1.compareTo(o2)).collect(Collectors.toList());
//			System.out.println("Sorted list: " + sortedList);

	}
}
